import { Component, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {
  recipes: Recipe[] = [
    new Recipe('Pizza Slice', 'pepperoni, onion', 'http://icons.iconarchive.com/icons/sonya/swarm/256/Pizza-icon.png'),
    new Recipe('Pizza Slice', 'pepperoni, onion', 'http://icons.iconarchive.com/icons/sonya/swarm/256/Pizza-icon.png'),
    new Recipe('Pizza Slice', 'pepperoni, onion', 'http://icons.iconarchive.com/icons/sonya/swarm/256/Pizza-icon.png')
  ];

  constructor() { }

  ngOnInit() {
  }

}
